﻿namespace Consileon
{
	public interface ICalculator
	{
		/// <summary>
		/// Adds two numbers
		/// </summary>
		/// <param name="a">First Number</param>
		/// <param name="b">Second Number</param>
		/// <returns>Sum of two numbers</returns>
		int Add(int a, int b);
		int Devide(int a, int b);
		int Multiply(int a, int b);
		int Subtract(int a, int b);
		double Sqrt(int a);
	}
}