﻿using Terrasoft.Core;

namespace ConsileonAPI
{
	public interface IConfToClio
	{
		void PostMessageToAll(string senderName, string messageText);
		void PostMessage(UserConnection userConnection, string senderName, string messageText);
	}
}
