using Terrasoft.Core;
using Terrasoft.Configuration;
using Terrasoft.Core.Factories;

namespace Consileon
{

	[DefaultBinding(typeof(ConsileonAPI.IConfToClio))]
	public class ConfToClio : ConsileonAPI.IConfToClio
	{
		/// <summary>
		/// Sends WebSocket message to all connected users
		/// </summary>
		/// <param name="senderName"></param>
		/// <param name="messageText"></param>
		public void PostMessageToAll(string senderName, string messageText)
		{
			MsgChannelUtilities.PostMessageToAll(senderName, messageText);
		}
		

		/// <summary>
		/// Send WebSocket message to  user identified by userConnection
		/// </summary>
		/// <param name="userConnection"></param>
		/// <param name="senderName"></param>
		/// <param name="messageText"></param>
		public void PostMessage(UserConnection userConnection, string senderName, string messageText)
		{
			MsgChannelUtilities.PostMessage(userConnection, senderName, messageText);
		}
	}
} 