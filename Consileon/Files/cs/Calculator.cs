﻿using System;
using Terrasoft.Core.Factories;

namespace Consileon
{
	[DefaultBinding(typeof(ICalculator))]
	public class Calculator : ICalculator
	{
		public int Add(int a, int b)
		{
			return a + b;
		}

		public int Subtract(int a, int b)
		{
			return a - b;
		}

		public int Multiply(int a, int b)
		{
			return a * b;
		}


		public int Devide(int a, int b)
		{
			return a / b;
		}

		public double Sqrt(int a)
		{
			return Math.Sqrt(a);
		}
	}
}
