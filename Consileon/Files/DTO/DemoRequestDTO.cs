﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Consileon
{
	[DataContract]
	public class DemoRequestDTO
	{
		[DataMember(Name="name")]
		public string Name { get; set; }

		[DataMember(Name = "birthday")]
		public string Birthday { get; set; }

		[DataMember(Name = "age")]
		public int Age { get; set; }
	}
}
