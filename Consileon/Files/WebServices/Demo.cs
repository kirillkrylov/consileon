﻿using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using Terrasoft.Core;
using Terrasoft.Web.Common;

namespace Consileon
{
	[ServiceContract]
	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
	public class Demo : BaseService
	{
		#region Properties
		private SystemUserConnection _systemUserConnection;
		private SystemUserConnection SystemUserConnection
		{
			get
			{
				return _systemUserConnection ?? (_systemUserConnection = (SystemUserConnection)AppConnection.SystemUserConnection);
			}
		}
		#endregion

		#region Methods : REST
		[OperationContract]
		[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
		public int PostMethodName(DemoRequestDTO demoRequestDTO)
		{


			return demoRequestDTO.Age + 10;
		}

		[OperationContract]
		[WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, 
			BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
		public string Get(string name)
		{
			UserConnection userConnection = UserConnection ?? SystemUserConnection;

			ConsileonAPI.IConfToClio conf = Terrasoft.Core.Factories.ClassFactory.Get<ConsileonAPI.IConfToClio>();
			conf.PostMessage(userConnection, GetType().Name, "Hello webSocket from clio through configuration");


			return $"hello, {name}";
		}

		#endregion

		#region Methods : Private

		#endregion
	}








}